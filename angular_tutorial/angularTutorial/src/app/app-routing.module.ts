import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//this is where we will do page routing
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';

const routes: Routes = [
{
  path:'',
  component:HomeComponent
},
{
  path:'about',
  component:AboutComponent
}

];//we set the paths here

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
