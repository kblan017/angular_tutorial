import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
}) //component decorator that defines attributes of components
export class AppComponent {
  title = 'app';//all component logic will reside here
}
