namespace DatingApp.API.Dtos
{
    public class UserForUpdateDto
    {
        // this class only has the properties we are allowing the user to edit

        public string Introduction {get;set;}
        public string LookingFor {get;set;}
        public string Interests {get;set;}
        public string City {get;set;}
        public string Country {get;set;}

    }
}