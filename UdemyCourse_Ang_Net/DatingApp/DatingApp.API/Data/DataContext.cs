using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
    public class DataContext : DbContext
    {
        // in api, through the terminal use 'dotnet ef migrations <className> , this allows you to create the entityframework you need'
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            
        }

        public DbSet<Value> Values {get;set;}

        public DbSet<User> Users {get;set;}

        public DbSet<Photo> Photos {get;set;}
    }
}