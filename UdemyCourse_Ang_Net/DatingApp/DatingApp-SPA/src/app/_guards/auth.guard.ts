import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
// created by being in the folder and using the terminal with commands 'ng g guard auth --spec=false' ,
// the spec is the test file that you can use but sometimes dont need
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  // tslint:disable-next-line:max-line-length

  constructor(private authService: AuthService, private router: Router, private alertify: AlertifyService) {

  }

  // tslint:disable-next-line:max-line-length
  canActivate(): boolean { // Observable<boolean> | Promise<boolean> | boolean these are union types, means you can return any of those values when piped '|'
  if (this.authService.loggedIn()) {
    return true;
  }

  this.alertify.error('You shall not pass!!!');
  this.router.navigate(['/home']);
  return false;

  }
}
